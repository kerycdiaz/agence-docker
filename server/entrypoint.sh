#!/bin/bash

echo "[run] go to project folder"
cd /home/app/server

echo "[run] Migrate DB"
python manage.py migrate --noinput

echo "[run] Collect static files"
python manage.py collectstatic --noinput

echo '[run] Load fixtures'
python manage.py loaddata ./*/fixtures/*.json