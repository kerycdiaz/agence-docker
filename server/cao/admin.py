from django.contrib import admin
from cao import models as cao_models

admin.site.register(cao_models.CaoUsuario)
admin.site.register(cao_models.PermissaoSistema)
admin.site.register(cao_models.CaoFatura)
admin.site.register(cao_models.CaoOs)