## Pre Requirements

1. install [docker](https://www.docker.com/).

## Installation

Automatic installation of the project with docker.

2. In `server` directory run `docker build -t server .` to build the Docker image.
3. To create a swarm `docker swarm init`.
4. Download all docker images:
    * `docker pull nginx:latest`  
    * `docker pull mysql:5.7`  
    * `docker pull dockersamples/visualizer:stable`  
    * `docker pull phpmyadmin/phpmyadmin`   
5. Run `docker stack deploy --compose-file=docker-compose.yml prod`
6. Open the browser at [http://localhost:8888](http://localhost:8888) to see your Django (server) app.
7. Open the browser at [http://localhost:8080](http://localhost:8080) to see the visualizer.
8. Open the browser at [http://localhost:8181](http://localhost:8181) to see the phpmyadmin.


## Our Stack

* [Django](https://www.djangoproject.com/)
* [MySQL](https://www.mysql.com/)
* [Docker](https://www.docker.com/)

**Tools we use**

  * [Docker Swarm](https://docs.docker.com/engine/swarm/)

## Rolling Updates

To update any of the containers that are in a service with a new image just create a new image, for example

```
docker build -t server:v2 .
```

And then update the service with the new image

```
docker service update --image server:v2 prod_web
```